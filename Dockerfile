FROM node:16 as dependencies
WORKDIR /web-service
COPY package.json yarn.lock ./
RUN yarn install --frozen-lockfile

FROM node:16 as builder
WORKDIR /web-service
COPY . .
COPY --from=dependencies /web-service/node_modules ./node_modules
RUN yarn build

FROM node:16 as runner
WORKDIR /web-service
ENV NODE_ENV production
COPY --from=builder /web-service/public ./public
COPY --from=builder /web-service/.next ./.next
COPY --from=builder /web-service/node_modules ./node_modules
COPY --from=builder /web-service/package.json ./package.json

EXPOSE 3000
CMD ["yarn", "start"]