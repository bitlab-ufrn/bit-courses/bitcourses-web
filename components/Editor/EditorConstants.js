import Header from "@editorjs/header"
import Paragraph from "@editorjs/paragraph"
import Embed from "@editorjs/embed"
import Table from "@editorjs/table"
import LinkTool from "@editorjs/link"
import Image from '@editorjs/image'
import Raw from "@editorjs/raw"
import Quote from "@editorjs/quote"
import Marker from "@editorjs/marker"
import CheckList from "@editorjs/checklist"
import Delimiter from "@editorjs/delimiter"
import InlineCode from "@editorjs/inline-code"
import SimpleImage from "@editorjs/simple-image"
import NestedList from "@editorjs/nested-list"
import mathTex from 'editorjs-math'
import CodeBox from '@bomdi/codebox'
import Warning from '@editorjs/warning'
import FootnotesTune from '@editorjs/footnotes';

const constants = {
  header: {
    class: Header,
    inlineToolbar: ["link"],
  },
  list: {
    class: NestedList,
    inlineToolbar: true,
  },
  paragraph: {
    class: Paragraph,
    inlineToolbar: true,
    tunes: ['footnotes'],
  },
  embed: Embed,
  table: {
    class: Table,
    inlineToolbar: true,
    config: {
      rows: 2,
      cols: 3,
    },
  },
  linkTool: LinkTool,
  image: Image,
  raw: Raw,
  quote: Quote,
  marker: Marker,
  checklist: CheckList,
  delimiter: Delimiter,
  inlineCode: InlineCode,
  simpleImage: SimpleImage,
  Math: {
    class: mathTex,
    shortcut: 'CMD+SHIFT+M'
  },
  
  warning: {
    class: Warning,
    inlineToolbar: true,
    shortcut: 'CMD+SHIFT+W',
    config: {
      titlePlaceholder: 'Title',
      messagePlaceholder: 'Message',
    },
  },
  codeBox: {
    class: CodeBox,
    config: {
      themeURL: 'https://cdn.jsdelivr.net/gh/highlightjs/cdn-release@9.18.1/build/styles/dracula.min.css', // Optional
      themeName: 'atom-one-dark', // Optional
      useDefaultTheme: 'light' // Optional. This also determines the background color of the language select drop-down
    }
  },
  footnotes: {
    class: FootnotesTune,
    config: {
      placeholder: 'Your placeholder for footnotes popover',
      shortcut: 'CMD+SHIFT+F',
    },
  }
};

export default constants;


