import { useCallback, useRef } from "react";
import { createReactEditorJS } from "react-editor-js";
import tools from "./EditorConstants"

import { data } from "./data"

let editorInstance;

const Editor = () => {

  const ReactEditorJS = createReactEditorJS();

  const editorJS = useRef(null);

  const handleInitialize = useCallback((instance) => {
    editorJS.current = instance;
  }, []);

  const handleSave = useCallback(async () => {
    const savedData = await editorJS.current.save();
    console.dir(savedData.blocks)
    console.log(JSON.stringify(savedData.blocks))
  }, []);

  return <div>
    <ReactEditorJS data={data} tools={tools} readOnly={false} onChange={handleSave} onInitialize={handleInitialize} />
    <button onClick={handleSave}> Save </button>
  </div>;
};

export default Editor;