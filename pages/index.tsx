import Head from 'next/head';
import Link from 'next/link';
import styles from '../styles/Home.module.css';

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Bitlab Courses</title>
        <meta name='description' content='Courses Platform from UFRN' />
        <link rel='icon' href='/favicon.ico' />
      </Head>

      <main className={styles.main}>
        
        <h1 className={styles.title}>Aqui será a tela inicial de cursos</h1>
        <div className={styles.grid}>
          <Link href='/class/'>
            <div className={styles.card}>
              <h2>Crie seu curso &rarr;</h2>
              <p>Começe agora a criar seu curso!</p>
            </div>
          </Link>

        </div>
      </main>
    </div>
  );
}
