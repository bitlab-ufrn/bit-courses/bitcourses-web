import dynamic from 'next/dynamic';
import styles from '../../styles/Class.module.css';

const Editor = dynamic(() => import('../../components/Editor/Editor'), { ssr: false });

const CreateClass = (props) => { 

  const onSaveHandler = async (data) => {

    // const toSave = {
    //     time: data.time,
    //     version: data.version,
    //     blocks: data.blocks
    // }

    //call API

    // console.log(editorInstance)

  };

  return (
    <div className={styles.page}>
      <Editor onSave={(editorData) => onSaveHandler(editorData)} />
      
    </div>
  );
};

export default CreateClass;



